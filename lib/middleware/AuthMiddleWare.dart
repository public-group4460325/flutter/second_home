import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../main.dart';

class AuthMiddleWare extends GetMiddleware {
  @override
  RouteSettings? redirect(String? route) {
    if (profile!.getString("userType") == "parent") {
      return const RouteSettings(name: "/HomeParent");
    }
    else if (profile!.getString("userType") == "student") {
      return const RouteSettings(name: "/Posts");
    }
    else if (profile!.getString("userType") == "visitor") {
      return const RouteSettings(name: "/OldExams");
    }
  }
}
