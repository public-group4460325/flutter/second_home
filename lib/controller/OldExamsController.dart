// import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import '../main.dart';

class OldExamsController extends GetxController {
  List Years = [];
  List Subjects = [];
  List Classes = [];
  var currentYear;
  var currentSubject;
  var currentClass;
  Future<List<dynamic>> Getinfo() async {
    final url = Uri.parse('${local}api/getinfo');
    var response = await http.post(url, body: {}, headers: {});
    if (response.statusCode == 200 || response.statusCode == 201) {
      var data = jsonDecode(response.body);
      Years = data[2];
      Classes = data[1];
      Subjects = data[0];
      return data;
    } else {
      Get.snackbar(
        "خطأ في الإتصال",
        "حاول مرة أخرى لاحقًا",
        backgroundColor: Colors.purple,
        colorText: Colors.white,
      );
      throw Exception('No Connection');
    }
  }

  Future<List<dynamic>> GetExams() async {
    final url = Uri.parse('${local}api/getexams');
    final body = {
      'year': currentYear ?? "",
      'subject': currentSubject != null ? currentSubject.toString() : '0',
      'class': currentClass != null ? currentClass.toString() : '0'
    };
    var response = await http.post(url, body: body, headers: {});
    if (response.statusCode == 200 || response.statusCode == 201) {
      var data = jsonDecode(response.body);
      return data;
    } else {
      Get.snackbar(
        "خطأ في الإتصال",
        "حاول مرة أخرى لاحقًا",
        backgroundColor: Colors.purple,
        colorText: Colors.white,
      );
      throw Exception('No Connection');
    }
  }

  ChangeYear(newYear) {
    currentYear = Years.firstWhere((element) => newYear == element);
    update();
  }

  ChangeSubject(newSubject) {
    currentSubject =
        Subjects.firstWhere((element) => newSubject == element[0])[0];
    update();
  }

  ChangeClass(newClass) {
    currentClass = Classes.firstWhere((element) => newClass == element[0])[0];
    update();
  }
}
