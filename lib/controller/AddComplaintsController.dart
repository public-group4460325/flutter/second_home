import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import '../main.dart';

class AddComplaintsController extends GetxController {
  TextEditingController complaintbody = TextEditingController();
  TextEditingController complainttitle = TextEditingController();
  GlobalKey<FormState> formState = GlobalKey<FormState>();
  Future<void> SendComplaint() async {
    var url = Uri.parse('${local}api/complints');
    var headers = {'Authorization': 'Bearer ${profile!.getString('token')}'};
    var data = {
      'title': complainttitle.text,
      'body': complaintbody.text,
      'user_name': profile!.getString('currentChild')
    };
    var response = await http.post(url, headers: headers, body: data);
    if (response.statusCode == 200 || response.statusCode == 201) {
      Get.snackbar(
        "تم الإرسال",
        "تم إرسال شكوى بنجاح",
        backgroundColor: Colors.purple,
        colorText: Colors.white,
      );
    } else {
      Get.snackbar(
        "خطأ في الإتصال",
        "حاول مرة أخرى لاحقًا",
        backgroundColor: Colors.purple,
        colorText: Colors.white,
      );
    }
  }
}
