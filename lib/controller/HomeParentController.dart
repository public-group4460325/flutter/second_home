import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import '../main.dart';

class HomeParentController extends GetxController {
  Future<List<dynamic>> GetChildren() async {
    final url = Uri.parse('${local}api/students_by_parent');
    final headers = {'Authorization': 'Bearer ${profile!.getString('token')}'};
    var response = await http.post(url, body: {}, headers: headers);
    if (response.statusCode == 200 || response.statusCode == 201) {
      var data = jsonDecode(response.body);
      return data;
      
    } else {
      Get.snackbar(
        "خطأ في الإتصال",
        "حاول مرة أخرى لاحقًا",
        backgroundColor: Colors.purple,
        colorText: Colors.white,
      );
      throw Exception('No Connection');
    }
  }
}
