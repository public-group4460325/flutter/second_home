import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'dart:convert';
import '../main.dart';
import 'package:http/http.dart' as http;

class LoginController extends GetxController {
  String? type;
  TextEditingController userName = TextEditingController();
  TextEditingController password = TextEditingController();
  GlobalKey<FormState> formState = GlobalKey<FormState>();

  bool check = true;
  ChangeColor() {
    check = !check;
    update();
  }

  Future<void> loginUser() async {
    Uri url = Uri.parse('${local}api/login');
    var response = await http.post(
      url,
      body: jsonEncode({'user_name': userName.text, 'password': password.text}),
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
      },
    );
    if (response.statusCode == 200 || response.statusCode == 201) {
      final jsonResponse = await jsonDecode(response.body);
      // profile!.setString("firstName", jsonResponse['user']['fistName']);
      // profile!.setString("lastName", jsonResponse['user']['lastName']);
      profile!.setString("userName", jsonResponse['user']['user_name']);
      // profile!.setString("phone", jsonResponse['user']['phone']);
      // profile!.setString("address", jsonResponse['user']['address']);
      profile!.setString("userType", jsonResponse['user']['type']);
      profile!.setString('token', jsonResponse['token']);
      profile!.getString("userType") == 'student'
          ? Get.offAllNamed("/Posts")
          : Get.offAllNamed("/HomeParent");
    } else {
      throw Exception('Failed to login');
    }
  }
}
