import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'view/Screen/Absences.dart';
import 'view/Screen/Premiums.dart';
import 'middleware/AuthMiddleWare.dart';
import 'view/Screen/HomeParent.dart';
import 'view/Screen/Marks.dart';
import 'view/Screen/OldExams.dart';
import 'view/Screen/Posts.dart';
import 'view/Screen/Schedule.dart';
import 'view/Screen/Events.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'view/Screen/Login.dart';
import 'view/Screen/Complaints.dart';
import 'view/Screen/AddComplaint.dart';

SharedPreferences? profile;
String local = 'http://10.0.2.2:8000/';

void main() async {
  WidgetsFlutterBinding();
  profile = await SharedPreferences.getInstance();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    profile!.getBool("isDarkMode") == null
        ? profile!.setBool("isDarkMode", false)
        : null;
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      theme: profile!.getBool("isDarkMode")!
          ? ThemeData.dark()
          : ThemeData.light(),
      getPages: [
        GetPage(name: "/", page: () => Login(), middlewares: [
          AuthMiddleWare(),
        ]),
        GetPage(name: "/HomeParent", page: () => HomeParent()),
        GetPage(name: "/Schedule", page: () => Schedule()),
        GetPage(name: "/Events", page: () => Events()),
        GetPage(name: "/Posts", page: () => Posts()),
        GetPage(name: "/OldExams", page: () => OldExams()),
        GetPage(name: "/Marks", page: () => Marks()),
        GetPage(name: "/Absences", page: () => Absences()),
        GetPage(name: "/Complaints", page: () => Complaints()),
        GetPage(name: "/AddComplaint", page: () => AddComplaint()),
        GetPage(name: "/Premiums", page: () => Premiums()),
      ],
    );
  }
}
