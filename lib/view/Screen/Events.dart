import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../controller/EventsController.dart';
import '../../../main.dart';
import '../widget/MainAppBar.dart';
import '../widget/MainDrawer.dart';

class Events extends StatelessWidget {
  const Events({super.key});

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: MainAppBar.mainAppBar(),
        drawer: profile!.getString("userType") == 'student'
            ? const StudentDrawer()
            : const FirstParentDrawer(),
        body: GetBuilder<EventsController>(
          init: EventsController(),
          builder: (controller) {
            return Center(
              child: FutureBuilder(
                future: controller.GetEvents(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return ListView.builder(
                      itemCount: snapshot.data?.length,
                      itemBuilder: (context, i) => Container(
                        color: const Color.fromARGB(61, 255, 238, 238),
                        margin: const EdgeInsets.all(10),
                        child: ListTile(
                          leading: const Icon(Icons.event),
                          isThreeLine: false,
                          title: Text(
                            snapshot.data![i]['title'],
                          ),
                          subtitle: Text(
                            snapshot.data![i]['body'],
                          ),
                          trailing: Column(
                            children: [
                              Text(
                                snapshot.data![i]['start_date'],
                              ),
                              Text(
                                snapshot.data![i]['end_date'],
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  }
                  return const CircularProgressIndicator(color: Colors.purple);
                },
              ),
            );
          },
        ),
      ),
    );
  }
}
