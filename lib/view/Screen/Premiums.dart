import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../controller/PremiumsController.dart';
import '../widget/MainAppBar.dart';
import '../widget/MainDrawer.dart';

class Premiums extends StatelessWidget {
  const Premiums({super.key});

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: MainAppBar.mainAppBar(),
        drawer: const FirstParentDrawer(),
        body: Column(
          children: [
            Container(
              decoration:
                  BoxDecoration(color: Color.fromARGB(255, 164, 74, 180)),
              child: const Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Text(
                    "التاريخ",
                    style: TextStyle(fontSize: 20),
                  ),
                  Text(
                    "الدفعة",
                    style: TextStyle(fontSize: 20),
                  ),
                  Text(
                    "الباقي",
                    style: TextStyle(fontSize: 20),
                  ),
                  Text(
                    "الكلي",
                    style: TextStyle(fontSize: 20),
                  ),
                ],
              ),
            ),
            Expanded(
              child: GetBuilder<PremiumsController>(
                init: PremiumsController(),
                builder: (controller) {
                  return Center(
                    child: FutureBuilder(
                      future: controller.GetPremiums(),
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          return ListView.builder(
                            itemCount: snapshot.data?.length,
                            itemBuilder: (context, i) => Column(
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                      color: const Color.fromARGB(
                                          255, 232, 130, 250)),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    children: [
                                      Text(
                                        snapshot.data![i]['date'].toString(),
                                        style: const TextStyle(fontSize: 20),
                                      ),
                                      Text(
                                        snapshot.data![i]['pay'].toString(),
                                        style: const TextStyle(fontSize: 20),
                                      ),
                                      Text(
                                        snapshot.data![i]['left'].toString(),
                                        style: const TextStyle(fontSize: 20),
                                      ),
                                      Text(
                                        snapshot.data![0]['total'].toString(),
                                        style: const TextStyle(fontSize: 20),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          );
                        }
                        return const CircularProgressIndicator(
                          color: Colors.purple,
                        );
                      },
                    ),
                  );
                },
              ),
            ),
            const SizedBox(
              height: 150,
            ),
            Opacity(
              opacity: .60,
              child: Image.asset(
                'Images/Logo.png',
                width: 300,
              ),
            ),
            const SizedBox(
              height: 100,
            )
          ],
        ),
      ),
    );
  }
}
