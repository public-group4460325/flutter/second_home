import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../widget/MainAppBar.dart';
import '../../controller/AddComplaintsController.dart';

class AddComplaint extends StatelessWidget {
  const AddComplaint({super.key});

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: MainAppBar.mainAppBar(),
        body: SingleChildScrollView(
          child: GetBuilder<AddComplaintsController>(
            init: AddComplaintsController(),
            builder: (controller) => Form(
              key: controller.formState,
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  children: [
                    const SizedBox(
                      height: 220,
                    ),
                    TextFormField(
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'هذا الحقل مطلوب.';
                        }
                        return null;
                      },
                      maxLines: 1,
                      maxLength: 30,
                      controller: controller.complainttitle,
                      decoration: const InputDecoration(
                        labelText: "إضافة عنوان الشكوى",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(
                            Radius.circular(8),
                          ),
                        ),
                        isDense: true,
                      ),
                    ),
                    TextFormField(
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'هذا الحقل مطلوب.';
                        }
                        return null;
                      },
                      maxLines: 4,
                      maxLength: 200,
                      controller: controller.complaintbody,
                      decoration: const InputDecoration(
                        labelText: "إضافة محتوى شكوى",
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(
                            Radius.circular(8),
                          ),
                        ),
                        isDense: true,
                      ),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Color.fromARGB(255, 134, 86, 216),
                        elevation: .0,
                        padding: EdgeInsets.all(0),
                      ),
                      onPressed: () {
                        if (controller.formState.currentState!.validate()) {
                          controller.SendComplaint();
                        }
                      },
                      child: const Text(
                        "  إضافة شكوى  ",
                        style: TextStyle(fontSize: 20),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
