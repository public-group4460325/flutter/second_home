import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../controller/LoginController.dart';
import '../widget/MainAppBar.dart';
import '../../main.dart';

class Login extends StatelessWidget {
  const Login({super.key});

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: MainAppBar.mainAppBar(),
        body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const SizedBox(
                height: 100,
              ),
              GetBuilder<LoginController>(
                init: LoginController(),
                builder: (controller) => Form(
                  key: controller.formState,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset("Images/Logo.png", width: 200),
                      Container(
                        margin: const EdgeInsets.only(
                            right: 10, left: 10, bottom: 10, top: 30),
                        child: TextFormField(
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'هذا الحقل مطلوب.';
                            }
                            return null;
                          },
                          controller: controller.userName,
                          decoration: InputDecoration(
                            filled: true,
                            icon: const Icon(Icons.person),
                            label: const Text("اسم المستخدم :"),
                            border: OutlineInputBorder(
                              borderSide: const BorderSide(
                                  color: Colors.blue, width: 3),
                              borderRadius: BorderRadius.circular(10),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderSide: const BorderSide(
                                  color: Colors.blue, width: 3),
                              borderRadius: BorderRadius.circular(10),
                            ),
                            errorBorder: OutlineInputBorder(
                              borderSide:
                                  const BorderSide(color: Colors.red, width: 3),
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(
                            right: 10, left: 10, bottom: 10, top: 5),
                        child: TextFormField(
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'هذا الحقل مطلوب.';
                            }
                            return null;
                          },
                          controller: controller.password,
                          obscureText: controller.check,
                          decoration: InputDecoration(
                            suffixIcon: IconButton(
                              onPressed: () {
                                controller.ChangeColor();
                              },
                              icon: Icon(
                                Icons.visibility,
                                color: controller.check == true
                                    ? Colors.grey
                                    : Colors.blue,
                              ),
                            ),
                            filled: true,
                            icon: const Icon(Icons.lock),
                            label: const Text("كلمة المرور :"),
                            border: OutlineInputBorder(
                              borderSide: const BorderSide(
                                  color: Colors.blue, width: 3),
                              borderRadius: BorderRadius.circular(10),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderSide: const BorderSide(
                                  color: Colors.blue, width: 3),
                              borderRadius: BorderRadius.circular(10),
                            ),
                            errorBorder: OutlineInputBorder(
                              borderSide:
                                  const BorderSide(color: Colors.red, width: 3),
                              borderRadius: BorderRadius.circular(10),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.only(left: 30),
                        alignment: Alignment.centerLeft,
                        child: ElevatedButton(
                          onPressed: () async {
                            if (controller.formState.currentState!.validate()) {
                              try {
                                await controller.loginUser();
                              } catch (e) {
                                ScaffoldMessenger.of(context).showSnackBar(
                                  SnackBar(
                                    content: Container(
                                      padding: const EdgeInsets.all(16),
                                      height: 90,
                                      decoration: BoxDecoration(
                                        color: Colors.purple,
                                        borderRadius: BorderRadius.circular(20),
                                      ),
                                      child: e
                                              .toString()
                                              .contains("Failed to login")
                                          ? const Text(
                                              "خطأ في كلمة المرور او اسم المستخدم",
                                              style: TextStyle(
                                                  color: Colors.white),
                                            )
                                          : const Text(
                                              "خطأ في الإتصال",
                                              style: TextStyle(
                                                  color: Colors.white),
                                            ),
                                    ),
                                    behavior: SnackBarBehavior.floating,
                                    backgroundColor: Colors.transparent,
                                  ),
                                );
                              }
                            }
                          },
                          style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.transparent,
                            elevation: .0,
                            padding: const EdgeInsets.all(0),
                          ),
                          child: Container(
                            padding: const EdgeInsets.only(
                                top: 10, bottom: 10, right: 20, left: 20),
                            decoration: const BoxDecoration(
                              borderRadius: BorderRadius.all(
                                Radius.circular(10),
                              ),
                              gradient: LinearGradient(
                                colors: [
                                  Colors.blue,
                                  Colors.purple,
                                ],
                              ),
                            ),
                            child: const Text(
                              "تسجيل الدخول",
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.only(left: 30),
                        alignment: Alignment.centerLeft,
                        child: ElevatedButton(
                          onPressed: () {
                            Get.offNamed('/OldExams');
                            profile!.setString("userType", 'visitor');
                          },
                          style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.transparent,
                            elevation: .0,
                            padding: const EdgeInsets.all(0),
                          ),
                          child: Container(
                            padding: const EdgeInsets.only(
                                top: 10, bottom: 10, right: 20, left: 20),
                            decoration: const BoxDecoration(
                              borderRadius: BorderRadius.all(
                                Radius.circular(10),
                              ),
                              gradient: LinearGradient(
                                colors: [
                                  Colors.blue,
                                  Colors.purple,
                                ],
                              ),
                            ),
                            child: const Text(
                              "الدخول كزائر",
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
