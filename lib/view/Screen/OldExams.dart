import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../controller/OldExamsController.dart';
import '../widget/MainAppBar.dart';
import '../widget/MyPosts.dart';
import '../widget/mainDrawer.dart';
import '../../main.dart';

class OldExams extends StatelessWidget {
  const OldExams({super.key});

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: MainAppBar.mainAppBar(),
        drawer: profile!.getString("userType") == 'student'
            ? const StudentDrawer()
            : const VisitorDrawer(),
        body: Column(
          children: [
            GetBuilder<OldExamsController>(
              init: OldExamsController(),
              builder: (controller) {
                controller.Getinfo();
                return FutureBuilder(
                  future: controller.Getinfo(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          DropdownButton(
                            hint: const Text("السنة"),
                            items: controller.Years.map(
                              (e) => DropdownMenuItem(
                                value: e,
                                child: Text("$e"),
                              ),
                            ).toList(),
                            onChanged: (value) {
                              controller.ChangeYear(value);
                              controller.update();
                              print(value);
                            },
                            value: controller.currentYear,
                          ),
                          DropdownButton(
                            hint: const Text("المادة"),
                            items: controller.Subjects.map(
                              (e) => DropdownMenuItem(
                                value: e[0],
                                child: Text("${e[1]}"),
                              ),
                            ).toList(),
                            onChanged: (value) {
                              print(value);
                              controller.ChangeSubject(value);
                            },
                            value: controller.currentSubject,
                          ),
                          DropdownButton(
                            hint: const Text("الصف"),
                            items: controller.Classes.map(
                              (e) => DropdownMenuItem(
                                value: e[0],
                                child: Text("${e[1]}"),
                              ),
                            ).toList(),
                            onChanged: (value) {
                              controller.ChangeClass(value);
                            },
                            value: controller.currentClass,
                          ),
                        ],
                      );
                    }
                    return const CircularProgressIndicator(
                      color: Colors.purple,
                    );
                  },
                );
              },
            ),
            Expanded(
              flex: 1,
              child: GetBuilder<OldExamsController>(
                init: OldExamsController(),
                builder: (controller) {
                  return Center(
                    child: FutureBuilder(
                      // key: controller.refreshKey,
                      future: controller.GetExams(),
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          return ListView.builder(
                            itemCount: snapshot.data?.length,
                            itemBuilder: (context, i) => Center(
                              child: MyPosts(
                                title: snapshot.data![i][3],
                                body: snapshot.data![i][1],
                                author: snapshot.data![i][2],
                                fileUrl: snapshot.data![i][0],
                                quizUrl: "",
                              ),
                            ),
                          );
                        }
                        return const CircularProgressIndicator(
                          color: Colors.purple,
                        );
                      },
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
