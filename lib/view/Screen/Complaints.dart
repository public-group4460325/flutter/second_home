import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../widget/MainAppBar.dart';
import '../widget/MainDrawer.dart';
import '../../controller/ComplaintsController.dart';

class Complaints extends StatelessWidget {
  const Complaints({super.key});

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: MainAppBar.mainAppBar(),
        drawer: const FirstParentDrawer(),
        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.blue,
          onPressed: () {
            Get.toNamed("/AddComplaint");
          },
          child: const Icon(Icons.add, color: Colors.white),
        ),
        body: Center(
          child: GetBuilder<ComplaintsController>(
            init: ComplaintsController(),
            builder: (controller) {
              return Center(
                child: FutureBuilder(
                  future: controller.GetComplaints(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return ListView.builder(
                        itemCount: snapshot.data!.length,
                        itemBuilder: (context, i) {
                          return Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(15),
                            ),
                            elevation: 7,
                            child: ListTile(
                              title: Text(
                                snapshot.data![i]["title"],
                              ),
                              subtitle: Text(
                                snapshot.data![i]["body"],
                              ),
                            ),
                          );
                        },
                      );
                    }
                    return const CircularProgressIndicator(
                      color: Colors.purple,
                    );
                  },
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
