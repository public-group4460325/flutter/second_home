import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../controller/AbcencesController.dart';
import '../widget/MainDrawer.dart';
import '../widget/MainAppBar.dart';

class Absences extends StatelessWidget {
  const Absences({super.key});

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: MainAppBar.mainAppBar(),
        drawer: const FirstParentDrawer(),
        body: GetBuilder<AbcencesController>(
          init: AbcencesController(),
          builder: (controller) {
            return Center(
              child: FutureBuilder(
                future: controller.GetAbcences(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return Column(
                      children: [
                        Expanded(
                          child: ListView.builder(
                            itemCount: snapshot.data!.length,
                            itemBuilder: (context, i) {
                              return Card(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                elevation: 7,
                                margin: const EdgeInsets.all(10),
                                child: Column(
                                  children: [
                                    Stack(
                                      children: [
                                        ClipRRect(
                                          borderRadius: const BorderRadius.only(
                                            topLeft: Radius.circular(15),
                                            topRight: Radius.circular(15),
                                          ),
                                          child: ListTile(
                                            title: Text(
                                              snapshot.data![i]['date'],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              );
                            },
                          ),
                        ),
                        const SizedBox(
                          height: 150,
                        ),
                        Opacity(
                          opacity: .60,
                          child: Image.asset(
                            'Images/Logo.png',
                            width: 300,
                          ),
                        ),
                        const SizedBox(
                          height: 100,
                        )
                      ],
                    );
                  }
                  return const CircularProgressIndicator(
                    color: Colors.purple,
                  );
                },
              ),
            );
          },
        ),
      ),
    );
  }
}
