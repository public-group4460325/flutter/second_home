import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../controller/ScheduleController.dart';
import '../widget/mainDrawer.dart';
import '../widget/MainAppBar.dart';

class Schedule extends StatelessWidget {
  const Schedule({super.key});

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: MainAppBar.mainAppBar(),
        drawer: const StudentDrawer(),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Container(
              decoration:
                  BoxDecoration(color: Color.fromARGB(255, 164, 74, 180)),
              child: const Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Text(
                    "الأحد",
                    style: TextStyle(fontSize: 20),
                  ),
                  Text(
                    "الإثنين",
                    style: TextStyle(fontSize: 20),
                  ),
                  Text(
                    "الثلاثاء",
                    style: TextStyle(fontSize: 20),
                  ),
                  Text(
                    "الأربعاء",
                    style: TextStyle(fontSize: 20),
                  ),
                  Text(
                    "الخميس",
                    style: TextStyle(fontSize: 20),
                  ),
                ],
              ),
            ),
            Expanded(
              child: GetBuilder<ScheduleController>(
                init: ScheduleController(),
                builder: (controller) {
                  return Center(
                    child: FutureBuilder(
                      future: controller.GetSchedule(),
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          return Container(
                            decoration: const BoxDecoration(
                                color: Color.fromARGB(255, 232, 130, 250)),
                            child: Row(
                              children: [
                                Expanded(
                                  child: ListView.builder(
                                    itemCount:
                                        snapshot.data![0]['sunday'].length,
                                    itemBuilder: (context, i) => Container(
                                      margin: const EdgeInsets.only(
                                          bottom: 5, top: 5, left: 3, right: 3),
                                      child: Text(
                                        "${snapshot.data![0]['sunday'][i]['order']}- ${snapshot.data![0]['sunday'][i]['subject'][0]}",
                                        style: const TextStyle(fontSize: 20),
                                      ),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: ListView.builder(
                                    itemCount:
                                        snapshot.data![0]['monday'].length,
                                    itemBuilder: (context, i) => Container(
                                      margin: const EdgeInsets.only(
                                          bottom: 5, top: 5, left: 3, right: 3),
                                      child: Text(
                                        "${snapshot.data![0]['monday'][i]['order']}- ${snapshot.data![0]['monday'][i]['subject'][0]}",
                                        style: const TextStyle(fontSize: 20),
                                      ),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: ListView.builder(
                                    itemCount:
                                        snapshot.data![0]['tuesday'].length,
                                    itemBuilder: (context, i) => Container(
                                      margin: const EdgeInsets.only(
                                          bottom: 5, top: 5, left: 3, right: 3),
                                      child: Text(
                                        "${snapshot.data![0]['tuesday'][i]['order']}- ${snapshot.data![0]['tuesday'][i]['subject'][0]}",
                                        style: const TextStyle(fontSize: 20),
                                      ),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: ListView.builder(
                                    itemCount:
                                        snapshot.data![0]['wednesday'].length,
                                    itemBuilder: (context, i) => Container(
                                      margin: const EdgeInsets.only(
                                          bottom: 5, top: 5, left: 3, right: 3),
                                      child: Text(
                                        "${snapshot.data![0]['wednesday'][i]['order']}- ${snapshot.data![0]['wednesday'][i]['subject'][0]}",
                                        style: const TextStyle(fontSize: 20),
                                      ),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: ListView.builder(
                                    itemCount:
                                        snapshot.data![0]['thursday'].length,
                                    itemBuilder: (context, i) => Container(
                                      margin: const EdgeInsets.only(
                                          bottom: 5, top: 5, left: 3, right: 3),
                                      child: Text(
                                        "${snapshot.data![0]['thursday'][i]['order']}- ${snapshot.data![0]['thursday'][i]['subject'][0]}",
                                        style: const TextStyle(fontSize: 20),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          );
                        }
                        return const CircularProgressIndicator(
                          color: Colors.purple,
                        );
                      },
                    ),
                  );
                },
              ),
            ),
            const SizedBox(
              height: 150,
            ),
            Opacity(
              opacity: .60,
              child: Image.asset(
                'Images/Logo.png',
                width: 300,
              ),
            ),
            const SizedBox(
              height: 100,
            )
          ],
        ),
      ),
    );
  }
}
