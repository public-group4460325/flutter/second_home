import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../controller/PostsController.dart';
import '../widget/MainAppBar.dart';
import '../widget/MyPosts.dart';
import '../widget/mainDrawer.dart';

class Posts extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: MainAppBar.mainAppBar(),
        drawer: const StudentDrawer(),
        body: GetBuilder<PostsController>(
          init: PostsController(),
          builder: (controller) {
            return Center(
              child: FutureBuilder(
                future: controller.GetPosts(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return ListView.builder(
                      itemCount: snapshot.data?.length,
                      itemBuilder: (context, i) => Center(
                        child: MyPosts(
                          title: snapshot.data![i]['title'],
                          body: snapshot.data![i]['body'],
                          author: snapshot.data![i]['teacher'],
                          fileUrl: snapshot.data![i]['file'] ?? "",
                          quizUrl: snapshot.data![i]['quizUrl'] ?? "",
                        ),
                      ),
                    );
                  }
                  return const CircularProgressIndicator(
                    color: Colors.purple,
                  );
                },
              ),
            );
          },
        ),
      ),
    );
  }
}
