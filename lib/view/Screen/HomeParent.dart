import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../main.dart';
import '../widget/mainDrawer.dart';
import '../widget/MainAppBar.dart';
import '../../controller/HomeParentController.dart';

class HomeParent extends StatelessWidget {
  const HomeParent({super.key});

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: MainAppBar.mainAppBar(),
        drawer: const SecondParentDrawer(),
        body: GetBuilder<HomeParentController>(
          init: HomeParentController(),
          builder: (controller) {
            return Center(
              child: FutureBuilder(
                future: controller.GetChildren(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return ListView.builder(
                      itemCount: snapshot.data?.length,
                      itemBuilder: (context, i) => InkWell(
                        child: Container(
                          color: const Color.fromARGB(61, 255, 238, 238),
                          margin: const EdgeInsets.all(10),
                          child: ListTile(
                            leading: const Icon(Icons.person_search_sharp),
                            isThreeLine: false,
                            title: Text(
                              snapshot.data![i]['name'],
                            ),
                            subtitle: Text(
                              snapshot.data![i]['class'],
                            ),
                            trailing: Text(
                              snapshot.data![i]['section'],
                            ),
                          ),
                        ),
                        onTap: () {
                          profile!.setString(
                            "currentChild",
                            snapshot.data![i]['name'],
                          );
                          Get.offNamed('/Complaints');
                        },
                      ),
                    );
                  }
                  return const CircularProgressIndicator(color: Colors.purple);
                },
              ),
            );
          },
        ),
      ),
    );
  }
}
