import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../controller/MarksController.dart';
import '../widget/MainAppBar.dart';
import '../widget/mainDrawer.dart';
import '../../main.dart';

class Marks extends StatelessWidget {
  const Marks({super.key});

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: MainAppBar.mainAppBar(),
        drawer: profile!.getString("userType") == 'student'
            ? StudentDrawer()
            : FirstParentDrawer(),
        body: Column(
          children: [
            Container(
              decoration:
                  BoxDecoration(color: Color.fromARGB(255, 164, 74, 180)),
              child: const Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Text(
                    "المادة",
                    style: TextStyle(fontSize: 20),
                  ),
                  Text(
                    "النظري",
                    style: TextStyle(fontSize: 20),
                  ),
                  Text(
                    "العملي",
                    style: TextStyle(fontSize: 20),
                  ),
                  Text(
                    "المجموع",
                    style: TextStyle(fontSize: 20),
                  ),
                ],
              ),
            ),
            Expanded(
              child: GetBuilder<MarksController>(
                init: MarksController(),
                builder: (controller) {
                  return Center(
                    child: FutureBuilder(
                      future: profile!.getString('userType') == 'student'
                          ? controller.GetMarks()
                          : controller.GetMarksParents(),
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          return ListView.builder(
                            itemCount: snapshot.data?.length,
                            itemBuilder: (context, i) => Column(
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                      color: const Color.fromARGB(
                                          255, 232, 130, 250)),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    children: [
                                      Text(
                                        snapshot.data![i]['subject'].toString(),
                                        style: const TextStyle(fontSize: 20),
                                      ),
                                      Text(
                                        snapshot.data![i]['theoretical_mark']
                                            .toString(),
                                        style: const TextStyle(fontSize: 20),
                                      ),
                                      Text(
                                        snapshot.data![i]['particular_mark']
                                            .toString(),
                                        style: const TextStyle(fontSize: 20),
                                      ),
                                      Text(
                                        (snapshot.data![i]['particular_mark'] +
                                                snapshot.data![i]
                                                    ['theoretical_mark'])
                                            .toString(),
                                        style: const TextStyle(fontSize: 20),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          );
                        }
                        return const CircularProgressIndicator(
                          color: Colors.purple,
                        );
                      },
                    ),
                  );
                },
              ),
            ),
            const SizedBox(
              height: 150,
            ),
            Opacity(
              opacity: .60,
              child: Image.asset(
                'Images/Logo.png',
                width: 300,
              ),
            ),
            const SizedBox(
              height: 100,
            )
          ],
        ),
      ),
    );
  }
}
