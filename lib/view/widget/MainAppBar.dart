import 'package:flutter/material.dart';

class MainAppBar {
  static AppBar mainAppBar() {
    return AppBar(
      title: const Text("بيتي الثاني"),
      backgroundColor: Colors.transparent,
      flexibleSpace: Container(
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            colors: [Colors.blue, Colors.purple],
          ),
        ),
      ),
    );
  }
}
