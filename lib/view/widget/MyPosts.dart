import 'dart:io';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:open_file/open_file.dart';
import 'package:dio/dio.dart';
import 'package:path_provider/path_provider.dart';
import '../../main.dart';

class MyPosts extends StatelessWidget {
  late String title;
  late String body;
  late String author;
  late String fileUrl;
  late String quizUrl;

  MyPosts({
    super.key,
    required this.title,
    required this.body,
    required this.author,
    required this.fileUrl,
    required this.quizUrl,
  });
  Future<void> downloadAndOpenPdf(String pdfUrl) async {
    Dio dio = Dio();
    Directory? downloadDirectory = Platform.isAndroid
        ? await getExternalStorageDirectory()
        : await getApplicationDocumentsDirectory();
    String fileName = fileUrl.split('/').last;
    String filePath = '${downloadDirectory!.path}/$fileName';
    print('${downloadDirectory.path}/$fileName');
    await dio.download(pdfUrl, filePath);
    await OpenFile.open(filePath);
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
      elevation: 4.0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
            padding: const EdgeInsets.all(16.0),
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  Colors.blue,
                  Colors.purple,
                ],
              ),
            ),
            child: Text(
              title,
              style: const TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20.0,
                color: Colors.white,
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  body,
                  style: const TextStyle(
                    fontSize: 16.0,
                  ),
                ),
                const SizedBox(height: 8.0),
                title != null && !title.isEmpty
                    ? Text(
                        '$author',
                        style: const TextStyle(
                          fontSize: 14.0,
                          color: Colors.grey,
                        ),
                      )
                    : const SizedBox.shrink(),
                const SizedBox(height: 16.0),
                fileUrl != null && !fileUrl.isEmpty
                    ? ElevatedButton(
                        onPressed: () async {
                          await downloadAndOpenPdf("${local}$fileUrl");
                        },
                        child: const Text(
                          'Open File',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      )
                    : const SizedBox.shrink(),
                const SizedBox(height: 1.0),
                quizUrl != null && !quizUrl.isEmpty
                    ? ElevatedButton(
                        onPressed: () async {
                          Uri url = Uri.parse(quizUrl);
                          await launchUrl(url);
                        },
                        child: const Text(
                          'Go To Quiz',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      )
                    : const SizedBox.shrink(),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
