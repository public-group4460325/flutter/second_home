import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../controller/AboutController.dart';
import '../../main.dart';

class StudentDrawer extends StatelessWidget {
  const StudentDrawer({super.key});

  LogOut() {
    bool? isDarkMode = profile!.getBool("isDarkMode");
    profile!.clear();
    profile!.setBool("isDarkMode", isDarkMode!);
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30), topLeft: Radius.circular(30))),
      child: Column(
        children: [
          Directionality(
            textDirection: TextDirection.ltr,
            child: UserAccountsDrawerHeader(
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(topLeft: Radius.circular(30)),
                  gradient:
                      LinearGradient(colors: [Colors.blue, Colors.purple]),
                ),
                accountName: Text("${profile!.getString("userName")}"),
                accountEmail: Text("${profile!.getString("userType")}")),
          ),
          ElevatedButton(
              style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.transparent,
                  shadowColor: Colors.transparent),
              onPressed: () {
                Get.offNamed("/Posts");
              },
              child: const ListTile(
                title: Text("المنشورات"),
                leading: Icon(
                  Icons.post_add,
                  color: Color(0xFF6E45e2),
                ),
              )),
          ElevatedButton(
              style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.transparent,
                  shadowColor: Colors.transparent),
              onPressed: () {
                Get.offNamed("/OldExams");
              },
              child: const ListTile(
                title: Text("الدورات السابقة"),
                leading: Icon(
                  Icons.assignment,
                  color: Color(0xFF6E45e2),
                ),
              )),
          ElevatedButton(
              style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.transparent,
                  shadowColor: Colors.transparent),
              onPressed: () {
                Get.offAllNamed('/Marks');
              },
              child: const ListTile(
                title: Text("العلامات"),
                leading: Icon(
                  Icons.home_work,
                  color: Color(0xFF6E45e2),
                ),
              )),
          ElevatedButton(
              style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.transparent,
                  shadowColor: Colors.transparent),
              onPressed: () {
                Get.offNamed("/Events");
              },
              child: const ListTile(
                title: Text("النقويم و المناسبات و العطل"),
                leading: Icon(
                  Icons.calendar_month,
                  color: Color(0xFF6E45e2),
                ),
              )),
          ElevatedButton(
              style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.transparent,
                  shadowColor: Colors.transparent),
              onPressed: () {
                Get.offNamed("/Schedule");
              },
              child: const ListTile(
                title: Text("برنامج الدوام"),
                leading: Icon(
                  Icons.class_rounded,
                  color: Color(0xFF6E45e2),
                ),
              )),
          ElevatedButton(
              style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.transparent,
                  shadowColor: Colors.transparent),
              onPressed: () {
                if (Get.isDarkMode) {
                  Get.changeTheme(ThemeData.light());
                  profile!.setBool("isDarkMode", false);
                } else {
                  Get.changeTheme(ThemeData.dark());
                  profile!.setBool("isDarkMode", true);
                }
              },
              child: const ListTile(
                title: Text("تغيير الثيم"),
                leading: Icon(
                  Icons.palette,
                  color: Color(0xFF6E45e2),
                ),
              )),
          ElevatedButton(
              style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.transparent,
                  shadowColor: Colors.transparent),
              onPressed: () {
                LogOut();
                Get.offAllNamed("/Login");
              },
              child: const ListTile(
                title: Text("تسجيل الخروج"),
                leading: Icon(
                  Icons.logout,
                  color: Color(0xFF6E45e2),
                ),
              )),
          Expanded(
            flex: 1,
            child: GetBuilder<AboutController>(
              init: AboutController(),
              builder: (controller) {
                return Center(
                  child: FutureBuilder(
                    future: controller.GetAbout(),
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        return Center(
                          child: Container(
                            child: ListView.builder(
                              itemCount: snapshot.data!.length,
                              itemBuilder: (context, i) {
                                return Center(
                                    child: Text(
                                        "${snapshot.data![i]['title']} : ${snapshot.data![i]['body']}"));
                              },
                            ),
                          ),
                        );
                      }
                      return const CircularProgressIndicator(
                        color: Colors.purple,
                      );
                    },
                  ),
                );
              },
            ),
          )
        ],
      ),
    );
  }
}

class FirstParentDrawer extends StatelessWidget {
  LogOut() {
    bool? isDarkMode = profile!.getBool("isDarkMode");
    profile!.clear();
    profile!.setBool("isDarkMode", isDarkMode!);
  }

  const FirstParentDrawer({super.key});

  @override
  Widget build(BuildContext context) {
    return Drawer(
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30), topLeft: Radius.circular(30))),
      child: Column(children: [
        Directionality(
          textDirection: TextDirection.ltr,
          child: UserAccountsDrawerHeader(
              decoration: const BoxDecoration(
                borderRadius: BorderRadius.only(topLeft: Radius.circular(30)),
                gradient: LinearGradient(colors: [Colors.blue, Colors.purple]),
              ),
              accountName: Text("${profile!.getString("userName")}"),
              accountEmail: Text("${profile!.getString("userType")}")),
        ),
        ElevatedButton(
            style: ElevatedButton.styleFrom(
                backgroundColor: Colors.transparent,
                shadowColor: Colors.transparent),
            onPressed: () {
              Get.toNamed("/HomeParent");
            },
            child: const ListTile(
              title: Text("الصفحة الرئيسية"),
              leading: Icon(
                Icons.home_work,
                color: Color(0xFF6E45e2),
              ),
            )),
        ElevatedButton(
            style: ElevatedButton.styleFrom(
                backgroundColor: Colors.transparent,
                shadowColor: Colors.transparent),
            onPressed: () {
              Get.offNamed("/Complaints");
            },
            child: const ListTile(
              title: Text("الشكاوي"),
              leading: Icon(
                Icons.wifi_protected_setup,
                color: Color(0xFF6E45e2),
              ),
            )),
        ElevatedButton(
            style: ElevatedButton.styleFrom(
                backgroundColor: Colors.transparent,
                shadowColor: Colors.transparent),
            onPressed: () {
              Get.toNamed("/Premiums");
            },
            child: const ListTile(
              title: Text("الأقساط"),
              leading: Icon(
                Icons.home_work,
                color: Color(0xFF6E45e2),
              ),
            )),
        ElevatedButton(
            style: ElevatedButton.styleFrom(
                backgroundColor: Colors.transparent,
                shadowColor: Colors.transparent),
            onPressed: () {
              Get.offAllNamed('/Marks');
            },
            child: const ListTile(
              title: Text("العلامات"),
              leading: Icon(
                Icons.home_work,
                color: Color(0xFF6E45e2),
              ),
            )),
        ElevatedButton(
            style: ElevatedButton.styleFrom(
                backgroundColor: Colors.transparent,
                shadowColor: Colors.transparent),
            onPressed: () {
              Get.toNamed("/Events");
            },
            child: const ListTile(
              title: Text("التقويم"),
              leading: Icon(
                Icons.calendar_month,
                color: Color(0xFF6E45e2),
              ),
            )),
        ElevatedButton(
            style: ElevatedButton.styleFrom(
                backgroundColor: Colors.transparent,
                shadowColor: Colors.transparent),
            onPressed: () {
              Get.toNamed("/Absences");
            },
            child: const ListTile(
              title: Text("استعراض الغياب"),
              leading: Icon(
                Icons.wallet_giftcard,
                color: Color(0xFF6E45e2),
              ),
            )),
        ElevatedButton(
            style: ElevatedButton.styleFrom(
                backgroundColor: Colors.transparent,
                shadowColor: Colors.transparent),
            onPressed: () {
              if (Get.isDarkMode) {
                Get.changeTheme(ThemeData.light());
                profile!.setBool("isDarkMode", false);
              } else {
                Get.changeTheme(ThemeData.dark());
                profile!.setBool("isDarkMode", true);
              }
            },
            child: const ListTile(
              title: Text("تغيير الثيم"),
              leading: Icon(
                Icons.palette,
                color: Color(0xFF6E45e2),
              ),
            )),
        ElevatedButton(
            style: ElevatedButton.styleFrom(
                backgroundColor: Colors.transparent,
                shadowColor: Colors.transparent),
            onPressed: () {
              LogOut();
              Get.offAllNamed("/Login");
            },
            child: const ListTile(
              title: Text("تسجيل الخروج"),
              leading: Icon(
                Icons.logout,
                color: Color(0xFF6E45e2),
              ),
            )),
        const SizedBox(
          height: 49.9,
        ),
        Expanded(
          flex: 1,
          child: GetBuilder<AboutController>(
            init: AboutController(),
            builder: (controller) {
              return Center(
                child: FutureBuilder(
                  future: controller.GetAbout(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return Center(
                        child: Container(
                          child: ListView.builder(
                            itemCount: snapshot.data!.length,
                            itemBuilder: (context, i) {
                              return Center(
                                  child: Text(
                                      "${snapshot.data![i]['title']} : ${snapshot.data![i]['body']}"));
                            },
                          ),
                        ),
                      );
                    }
                    return const CircularProgressIndicator(
                      color: Colors.purple,
                    );
                  },
                ),
              );
            },
          ),
        )
      ]),
    );
  }
}

class SecondParentDrawer extends StatelessWidget {
  const SecondParentDrawer({super.key});

  LogOut() {
    bool? isDarkMode = profile!.getBool("isDarkMode");
    profile!.clear();
    profile!.setBool("isDarkMode", isDarkMode!);
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30), topLeft: Radius.circular(30))),
      child: Column(
        children: [
          Directionality(
            textDirection: TextDirection.ltr,
            child: UserAccountsDrawerHeader(
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(topLeft: Radius.circular(30)),
                  gradient:
                      LinearGradient(colors: [Colors.blue, Colors.purple]),
                ),
                accountName: Text("${profile!.getString("userName")}"),
                accountEmail: Text("${profile!.getString("userType")}")),
          ),
          ElevatedButton(
            style: ElevatedButton.styleFrom(
                backgroundColor: Colors.transparent,
                shadowColor: Colors.transparent),
            onPressed: () {
              if (Get.isDarkMode) {
                Get.changeTheme(ThemeData.light());
                profile!.setBool("isDarkMode", false);
              } else {
                Get.changeTheme(ThemeData.dark());
                profile!.setBool("isDarkMode", true);
              }
            },
            child: const ListTile(
              title: Text("تغيير الثيم"),
              leading: Icon(
                Icons.palette,
                color: Color(0xFF6E45e2),
              ),
            ),
          ),
          ElevatedButton(
              style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.transparent,
                  shadowColor: Colors.transparent),
              onPressed: () {
                LogOut();
                Get.offAllNamed("/Login");
              },
              child: const ListTile(
                title: Text("تسجيل الخروج"),
                leading: Icon(
                  Icons.logout,
                  color: Color(0xFF6E45e2),
                ),
              )),
          const SizedBox(
            height: 49.9,
          ),
          Expanded(
            flex: 1,
            child: GetBuilder<AboutController>(
              init: AboutController(),
              builder: (controller) {
                return Center(
                  child: FutureBuilder(
                    future: controller.GetAbout(),
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        return Center(
                          child: Container(
                            child: ListView.builder(
                              itemCount: snapshot.data!.length,
                              itemBuilder: (context, i) {
                                return Center(
                                    child: Text(
                                        "${snapshot.data![i]['title']} : ${snapshot.data![i]['body']}"));
                              },
                            ),
                          ),
                        );
                      }
                      return const CircularProgressIndicator(
                        color: Colors.purple,
                      );
                    },
                  ),
                );
              },
            ),
          )
        ],
      ),
    );
  }
}

class VisitorDrawer extends StatelessWidget {
  const VisitorDrawer({super.key});

  LogOut() {
    bool? isDarkMode = profile!.getBool("isDarkMode");
    profile!.clear();
    profile!.setBool("isDarkMode", isDarkMode!);
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30), topLeft: Radius.circular(30))),
      child: Column(
        children: [
          const Directionality(
            textDirection: TextDirection.ltr,
            child: UserAccountsDrawerHeader(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(topLeft: Radius.circular(30)),
                  gradient:
                      LinearGradient(colors: [Colors.blue, Colors.purple]),
                ),
                accountName: Text(""),
                accountEmail: Text("")),
          ),
          ElevatedButton(
              style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.transparent,
                  shadowColor: Colors.transparent),
              onPressed: () {
                Get.offNamed("/HomeVisitor");
              },
              child: const ListTile(
                title: Text("استعراض النماذج الإمتحانية"),
                leading: Icon(
                  Icons.wallet_giftcard,
                  color: Color(0xFF6E45e2),
                ),
              )),
          ElevatedButton(
            style: ElevatedButton.styleFrom(
                backgroundColor: Colors.transparent,
                shadowColor: Colors.transparent),
            onPressed: () {
              if (Get.isDarkMode) {
                Get.changeTheme(ThemeData.light());
                profile!.setBool("isDarkMode", false);
              } else {
                Get.changeTheme(ThemeData.dark());
                profile!.setBool("isDarkMode", true);
              }
            },
            child: const ListTile(
              title: Text("تغيير الثيم"),
              leading: Icon(
                Icons.palette,
                color: Color(0xFF6E45e2),
              ),
            ),
          ),
          ElevatedButton(
              style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.transparent,
                  shadowColor: Colors.transparent),
              onPressed: () {
                LogOut();
                Get.offAllNamed("/Login");
              },
              child: const ListTile(
                title: Text("تسجيل الدخول"),
                leading: Icon(
                  Icons.logout,
                  color: Color(0xFF6E45e2),
                ),
              )),
          const SizedBox(
            height: 49.9,
          ),
          Expanded(
            flex: 1,
            child: GetBuilder<AboutController>(
              init: AboutController(),
              builder: (controller) {
                return Center(
                  child: FutureBuilder(
                    future: controller.GetAbout(),
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        return Center(
                          child: Container(
                            child: ListView.builder(
                              itemCount: snapshot.data!.length,
                              itemBuilder: (context, i) {
                                return Center(
                                  child: Text(
                                      "${snapshot.data![i]['title']} : ${snapshot.data![i]['body']}"),
                                );
                              },
                            ),
                          ),
                        );
                      }
                      return const CircularProgressIndicator(
                        color: Colors.purple,
                      );
                    },
                  ),
                );
              },
            ),
          )
        ],
      ),
    );
  }
}
